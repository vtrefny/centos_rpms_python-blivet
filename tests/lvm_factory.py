#!/usr/bin/python3

import argparse
import sys

import blivet


POOL_NAME = "blivet_test"

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--disks', default='')
args = parser.parse_args()

b = blivet.Blivet()
b.reset()

disks = list()
for disk_id in args.disks.split(','):
    disk = b.devicetree.resolve_device(disk_id)
    if not disk.is_disk:
        sys.stderr.write("specified disk '%s' is not a disk\n" % disk_id)
        sys.exit(1)

    disks.append(disk)
    b.initialize_disk(disk)

if len(disks) > 1:
    container_raid_level = "raid1"
    total_size = min(d.size for d in disks)
else:
    container_raid_level = None
    total_size = sum(d.size for d in disks)

lv1 = b.factory_device(size=total_size*0.8, disks=disks,
                       name="lv1", container_name=POOL_NAME,
                       fstype='xfs', device_type=blivet.devicefactory.DEVICE_TYPE_LVM)
lv2 = b.factory_device(disks=disks, name="lv2",
                       container_name=POOL_NAME, container_raid_level='raid1',
                       fstype='ext4', device_type=blivet.devicefactory.DEVICE_TYPE_LVM)

b.do_it()
